# gomautic

## Introduction
Golang client for Mautic REST API

Only works with Basic Authentication

## Usage
```
client := gomautic.New("username", "password", "https://mautic-endpoint.pt")

contactId := 12345
contact, err := client.GetContactById(contactId)
if err != nil {
    fmt.Printf("Error getting contact %d", contactId)
    os.Exit(1)
}
```