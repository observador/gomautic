package gomautic

import (
	"io"
	"io/ioutil"
	"net/http"
)

type API struct {
	Username string
	Password string
	Endpoint string
}

//New creates a API
func New(username string, password string, endpoint string) *API {
	return &API{
		Username: username,
		Password: password,
		Endpoint: endpoint + "/api",
	}
}

func (api *API) request(method string, url string, body io.Reader, params map[string]string) (statusCode int, responseBody []byte, err error) {
	req, err := http.NewRequest(method, url, body)
	if err != nil {
		return
	}

	req.Header.Set("Content-type", "application/json")
	req.SetBasicAuth(api.Username, api.Password)

	queryParams := req.URL.Query()

	for k, v := range params {
		queryParams.Set(k, v)
	}

	req.URL.RawQuery = queryParams.Encode()

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return
	}
	defer resp.Body.Close()

	responseBody, err = ioutil.ReadAll(resp.Body)
	if err != nil {
		return
	}
	statusCode = resp.StatusCode

	return
}
