package gomautic

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"strconv"
)

type ContactsWrapper struct {
	Total    string             `json:"total"`
	Contacts map[string]Contact `json:"contacts"`
}

type ContactWrapper struct {
	Contact Contact `json:"contact"`
}

type Contact struct {
	Id          int    `json:"id"`
	IsPublished bool   `json:"isPublished"`
	DateAdded   string `json:"dateAdded"`
	Points      int    `json:"points"`
	Fields      Fields `json:"fields"`
}

type Fields struct {
	Core   map[string]Field `json:"core"`
	Social map[string]Field `json:"social"`
}

type Field struct {
	Id              interface{} `json:"id"` //interface{} because sometimes is int, and sometimes is string...
	Label           string      `json:"label"`
	Alias           string      `json:"alias"`
	Type            string      `json:"type"`
	Group           string      `json:"group"`
	Object          string      `json:"object"`
	IsFixed         string      `json:"is_fixed"`
	Properties      interface{} `json:"properties"`      //interface{} because sometimes is object, and sometimes is string...
	DefaultValue    interface{} `json:"default_value"`   //interface{} because can have multiple types...
	Value           interface{} `json:"value"`           //interface{} because can have multiple types...
	NormalizedValue interface{} `json:"normalizedValue"` //interface{} because can have multiple types...
}

//GetContacts returns contacts
//
//params: read mautic documentation
func (api *API) GetContacts(start int, limit int, params map[string]string) (contacts ContactsWrapper, err error) {
	urls := fmt.Sprintf("%s/contacts",
		api.Endpoint)

	if params == nil {
		params = make(map[string]string)
	}
	params["start"] = strconv.Itoa(start)
	params["limit"] = strconv.Itoa(limit)

	_, body, err := api.request("GET", urls, nil, params)
	if err != nil {
		return
	}

	err = json.Unmarshal(body, &contacts)
	if err != nil {
		err = nil //Ignore error because when there are no contacts mautic api returns an Array instead of a Map...
	}

	return
}

//GetContactByEmail returns contact by email
//
//If no contact found, Id is '0'
func (api *API) GetContactByEmail(email string) (contact Contact, err error) {
	params := make(map[string]string)
	params["where[0][col]"] = "email"
	params["where[0][expr]"] = "eq"
	params["where[0][val]"] = email

	contacts, err := api.GetContacts(0, 1, params)
	if err != nil {
		return
	} else {
		total, errConv := strconv.Atoi(contacts.Total)
		if errConv != nil {
			return
		} else {
			if total > 0 {
				for _, v := range contacts.Contacts {
					contact = v
				}
			}
		}
	}
	return
}

//GetContactById returns contact by id
//
//If no contact found, Id is '0'
func (api *API) GetContactById(id int) (contact ContactWrapper, err error) {
	url := fmt.Sprintf("%s/contacts/%d",
		api.Endpoint,
		id)

	_, body, err := api.request("GET", url, nil, nil)
	if err != nil {
		return
	}

	err = json.Unmarshal(body, &contact)
	if err != nil {
		return
	}

	return
}

//UpdateContactById updates a contact by id
func (api *API) UpdateContactById(id int, fields map[string]interface{}, tags map[string]bool) (contact ContactWrapper, err error) {
	url := fmt.Sprintf("%s/contacts/%d/edit",
		api.Endpoint,
		id)

	if fields == nil {
		fields = make(map[string]interface{})
	}
	addTagsToFields(fields, tags)

	reqBody, err := json.Marshal(fields)
	if err != nil {
		return
	}

	_, body, err := api.request("PATCH", url, bytes.NewBuffer(reqBody), nil)
	if err != nil {
		return
	}

	err = json.Unmarshal(body, &contact)
	if err != nil {
		return
	}

	return
}

//UpdateContactByEmail updates a contact by email
func (api *API) UpdateContactByEmail(email string, fields map[string]interface{}, tags map[string]bool) (contact ContactWrapper, err error) {
	userContact, errGetContact := api.GetContactByEmail(email)
	if errGetContact != nil {
		err = errGetContact
		return
	}

	if userContact.Id == 0 {
		err = errors.New(fmt.Sprintf("no contact found with email '%s'", email))
		return
	}

	return api.UpdateContactById(userContact.Id, fields, tags)
}

//CreateContact creates a contact
func (api *API) CreateContact(email string, fields map[string]interface{}, tags map[string]bool) (contact ContactWrapper, err error) {
	url := fmt.Sprintf("%s/contacts/new",
		api.Endpoint)

	if fields == nil {
		fields = make(map[string]interface{})
	}
	fields["email"] = email
	addTagsToFields(fields, tags)

	reqBody, err := json.Marshal(fields)
	if err != nil {
		return
	}

	_, body, err := api.request("POST", url, bytes.NewBuffer(reqBody), nil)
	if err != nil {
		return
	}

	err = json.Unmarshal(body, &contact)
	if err != nil {
		return
	}

	return
}

func addTagsToFields(fields map[string]interface{}, tags map[string]bool) {
	if tags != nil {
		if len(tags) > 0 {
			var tagsArr []string
			for tag, isAdd := range tags {
				if isAdd {
					tagsArr = append(tagsArr, tag)
				} else {
					tagsArr = append(tagsArr, "-"+tag)
				}
			}
			fields["tags"] = tagsArr
		}
	}
}
